trait StateMachine {
    nextState() -> StateMachine;
    enable() -> ();
    disable() -> ();
    update() -> ();
    finished() -> bool;
    progress() -> double;
    description() -> String;
}