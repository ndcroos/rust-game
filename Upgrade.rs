trait Upgrade {
    fn enable() -> ();
    fn disable() -> ();
    fn update() -> ();
    fn finished() -> bool;
    fn enabled() -> bool;
}